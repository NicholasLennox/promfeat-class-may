﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PromFeatMayPrep
{
    internal struct WeatherPoint
    {
        public int Temperature { get; set; }
        public DateTime Date { get; set; }

        public override string? ToString()
        {
            return $"Temp: {Temperature}, Date: {Date}";
        }
    }
}
