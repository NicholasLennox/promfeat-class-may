﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PromFeatMayPrep
{
    // Has to be static class
    internal static class IntExtensions
    {
        // Has to be static method, 'this' refers to the int struct that we are extending
        public static int RollTheDice(this int value) 
        {
            // Just returns a random number, with a max being the number its called on.
            Random random = new Random();
            return random.Next(value); 
        }
    }
}
