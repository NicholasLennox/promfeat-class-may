﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PromFeatMayPrep
{
    // Final 1
    internal static class ListExtensions
    {
        public static List<T> Filter<T>(this List<T> source, Predicate<T> predicate) // What if we wanted any enumerable
        {
            List<T> filtered = new List<T>(); // Also want to improve this, dont want a temp list.
            foreach (var item in source)
            {
                if (predicate(item))
                    filtered.Add(item);
            }
            return filtered;
        }
    }
}
