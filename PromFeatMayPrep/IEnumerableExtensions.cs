﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PromFeatMayPrep
{
    // Final 3
    // Yield - https://learn.microsoft.com/en-us/dotnet/csharp/language-reference/statements/yield
    internal static class IEnumerableExtensions
    {
        // This is an iterator pattern
        public static IEnumerable<T> MyFilter<T>(this IEnumerable<T> source, Predicate<T> check) // Can change to func
        {
            // Early exit, if source null
            if (source == null)
                throw new Exception("Source is null");
            var enumerator = source.GetEnumerator();
            while (enumerator.MoveNext())
            {
                var item = enumerator.Current;
                if (check(item))
                    yield return item;
            }
        }
    }
}
