﻿using System.Collections.Generic;
using System.ComponentModel;

namespace PromFeatMayPrep
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello, World!");
            // Ext 1
            List<string> list = new List<string>();
            var unique = list.Distinct();
            // Ext 2
            int number = 10;
            int newNumber = number.RollTheDice();
            // Delegate 3
            CheckWeatherWarning freezing = IsFreezing;
            CheckWeatherWarning heatwave = IsHeatwave;
            // Delegate 4
            if(freezing(10))
                Console.WriteLine("Its too cold");
            if (heatwave(35))
                Console.WriteLine("Its too hot");
            // Delegate 7
            List<WeatherPoint> points = new List<WeatherPoint>() // Weeks worth of weather data
            {
                new WeatherPoint() { Temperature = 10,  Date = DateTime.Today.AddDays(-6) },
                new WeatherPoint() { Temperature = 15,  Date = DateTime.Today.AddDays(-5) },
                new WeatherPoint() { Temperature = 18,  Date = DateTime.Today.AddDays(-4) },
                new WeatherPoint() { Temperature = 32,  Date = DateTime.Today.AddDays(-3) },
                new WeatherPoint() { Temperature = 25,  Date = DateTime.Today.AddDays(-2) },
                new WeatherPoint() { Temperature = 12,  Date = DateTime.Today.AddDays(-1) },
                new WeatherPoint() { Temperature = -2,  Date = DateTime.Today },
            };
            var freezingDays = FilterWeather(points, freezing); // Use filter for freezing
            var heatwaveDays = FilterWeather(points, heatwave); // and for heatwave
            // Generic 1
            PrintList(freezingDays);
            // Generic 4
            PrintEnumerable(heatwaveDays);
            int[] temps = { 10, 12, 15, 18 };
            PrintEnumerable(temps); // Not limited to lists for IEnumerable 
            // Lambda 1
            var filtered = FilterWeather(points, t => t > 15); // Using lambda instead of our methods. A lot more flexible.
            // LINQ 1
            Console.WriteLine("\nLINQ 1\n");
            var notFreezingLinq = from point in points
                           where point.Temperature > 0
                           orderby point.Temperature
                           select point.Temperature;
            PrintEnumerable(notFreezingLinq);
            // LINQ 2
            Console.WriteLine("\nLINQ 2\n");
            var notFreezingExt = points
                .Where(t => t.Temperature > 0)
                .OrderBy(t => t.Temperature)
                .Select(t => t.Temperature);
            PrintEnumerable(notFreezingExt);
            // Func - https://learn.microsoft.com/en-us/dotnet/api/system.func-2?view=net-6.0
            // Action - https://learn.microsoft.com/en-us/dotnet/api/system.action?view=net-6.0
            // Predicate - https://learn.microsoft.com/en-us/dotnet/api/system.predicate-1?view=net-6.0

            // Final 2
            var filteredPoints = points.Filter(p => p.Temperature > 0);
            // Final 4
            var filteredPointsExt = points.MyFilter(p => p.Temperature > 0);
        }

        // Generic 2
        private static void PrintList<T>(List<T> list)
        {
            foreach (T item in list)
            {
                Console.WriteLine(item);
            }
        }

        // Generic 3
        public static void PrintEnumerable<T>(IEnumerable<T> values)
        {
            foreach (T value in values)
            {
                Console.WriteLine(value);
            }
        }

        // Delegate 5
        public static List<WeatherPoint> FilterWeatherFreezing(List<WeatherPoint> points) // No delegate
        {
            List<WeatherPoint> filtered = new List<WeatherPoint>();
            foreach (var point in points)
            {
                if (IsFreezing(point.Temperature)) // Limited to one kind of filter
                    filtered.Add(point);
            }
            return filtered;
        }

        // Delegate 6
        public static List<WeatherPoint> FilterWeather(List<WeatherPoint> points, CheckWeatherWarning check) // With delegate
        {
            List<WeatherPoint> filtered = new List<WeatherPoint>();
            foreach(var point in points)
            {
                if(check(point.Temperature)) // Can use anything that fits it
                    filtered.Add(point);
            }
            return filtered;
        }

        // Delegate 2
        public static bool IsFreezing(int temperature)
        {
            return temperature < 0;
        }

        public static bool IsHeatwave(int temperature)
        {
            return temperature > 30;
        }

        // Delegate 1
        public delegate bool CheckWeatherWarning(int temperature);
    }
}